# interface-lammps-mlip-v2

An interface between LAMMPS and MLIP.
MLIP, Machine Learning Interatomic Potentials, is a software distributed
by the group of A. Shapeev from Skoltech (Moscow)

## Installation

In order to install a MLIP plugin into LAMMPS:

1. Compile `lib_mlip_interface.a` and place it in this folder
(together with `./preinstall.sh`)

2. Run `./preinstall.sh <path-to-lammps>`

3. Go and compile LAMMPS in <path-to-lammps>.
The binary created in <path-to-lammps>/src/ has MLIP in it

Instead of steps (2) and (3) you can run

```./install.sh <path-to-lammps> <lammps-target>```

e.g.

```./install.sh ~/lammps/lammps-22Aug18 intel_cpu_intelmpi```

If everything goes fine, the lammps binary will be copied to this folder

### Combination of LAMMPS and MLIP settings that work:

* MLIP with the GNU compiler and LAMMPS with `make g++_serial` 
* MLIP with the GNU compiler and mpich and LAMMPS with `make g++_mpich` 
* MLIP with Intel compiler and intel MPI and LAMMPS with `make intel_cpu_intelmpi`
* If you'd like to build with Intel and no MPI then you need to find a way
to link LAMMPS with MKL
